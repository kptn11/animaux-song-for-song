<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.cdnfonts.com/css/lion-king" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Homepage</title>
</head>
<body>
    <?php
    include "partials/header.php";
    include_once "../model/data.php";
    ?>
<div id="videosContainer">
    <div id="videoPrincipal">
        <p id="phraseAccroche">Créer vortre musique avec les sons des animaux !</p>
        <video class="videoPrinc" controls>
        <source src="../video/animauxSong.mp4" type="video/mp4">
        Votre navigateur ne supporte pas la balise video.
    </video>
    </div>
    <div id="videoSecCont">
        <video class="videoSec"controls>
        <source src="../video/chatQuiChante1.mp4" type="video/mp4">
        Votre navigateur ne supporte pas la balise video.
        </video>
        <video class="videoSec"controls>    
        <source src="../video/chienQuiChante1.mp4" type="video/mp4">
        Votre navigateur ne supporte pas la balise video.
        </video>
        <video class="videoSec"controls>    
        <source src="../video/chatQuiChante2.mp4" type="video/mp4">
        Votre navigateur ne supporte pas la balise video.
        </video>
        <video class="videoSec"controls>    
        <source src="../video/chienQuiChante2.mp4" type="video/mp4">
        Votre navigateur ne supporte pas la balise video.
        </video>
    </div> 
</div>   
</body>
</html>