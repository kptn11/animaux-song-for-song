<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.cdnfonts.com/css/lion-king" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Boutique</title>
</head>
<body>
    <?php 
        include "../model/data.php";
        include "partials/header.php";
        ?>

        <?php
        include "partials/listeProduit.php";
        ?>
        </div>
    
</body>
</html>