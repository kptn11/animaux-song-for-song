<?php
    include_once "../model/data.php";

    $mesinfos =  readallproduit();
    ?>
    <div id="produitCont">
    <?php
    foreach($mesinfos as $data){ 
        $son = $data['sonMP3'];
        ?>
        <div class="border, produit">
            <img class="imgcymb" src="<?= $data["imgURL"] ?>" >
            <div class="playprixbtnnom">
                <div class="img-nom">
                    <h2 class="nomcymb"><?= $data["nom"] ?></h2>
                </div>
                <div class="info"><?= $data["info"] ?></div>
                <div class="playprixbtn">
                    <audio id="player"class="myAudio" controls width="300" height="32">
                    <source src='<?= $son ?>' type="audio/mpeg"> </audio>
                    <div class="prix"> <?= $data["prix"] ?> €</div>
                </div>
                
                <div> <?php if($data["dispo"] == 0){ ?>
                    <p>Indisponible ! Quelqu'un ta devancer ! Cheh !!!</p>
                </div>
                    <?php } else { ?>
            <div>
                <form action="../control/ajouterPanier.php" method="post">
                    <input type="hidden" name="id_produit" value="<?= $data["id"] ?>">
                    <input type="hidden" name="dispo" value="<?= $data["dispo"] ?>">
                    <input id="ajouterPanier" type="submit" value="Ajouter au panier">
                </form>
            </div> 
            <?php } ?>
            
            <?php 
            if($_SESSION["admin"]){ ?>
                <a href="partials/formUpdtProduit.php?id=<?= $data["id"]; ?>">Modifier</a>
                <a href="../control/deleteProduit.php?id=<?= $data["id"]; ?>">Supprimer</a>
                <?php } ?>
    </div>
    <script>

        function playAudio(son){

            let player = document.getElementById("player");
            player.son = son;
            player.play();

        }
    </script>
    
    </div>
    </div>
    </div>
    </div>
    <?php } ?>