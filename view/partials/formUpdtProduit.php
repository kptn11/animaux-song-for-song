<?php
    include_once "../../model/data.php";
    // recupération de l'id du produit que l'on veut modofier
    $id = $_GET["id"];
    // appelle de la function qui lit, avec l'id correspondant au produit choisie 
    $produit = readproduitbyid($id)[0];
?>
<script>
    var ray={
ajax:function(st)
    {
        this.show('load');
    },
show:function(el)
    {
        this.getID(el).style.display='';
    },
getID:function(el)
    {
        return document.getElementById(el);
    }
}
</script>
<!-- formulaire de modification de la produitale -->
<form enctype="multipart/form-data" action="../../control/modifierProduit.php" method = "post" onsubmit="return ray.ajax()">
    <div>
        <input type="hidden" name='id' value="<?= $produit["id"]; ?>">
    </div>
    <div>
        <label for="nom">Nom de l'animal :</label>
        <input type="text" name="nom" value="<?=$produit["nom"];?>">
    </div>
    <div>
        <label for="imgURL">URL de l'image :</label>
        <input type="text" name="imgURL"  value="<?=$produit["imgURL"];?>">
    </div>
    <div>
        <label for="info">Breves infos :</label>
        <input type="text" name="info" value="<?=$produit["info"];?>">
    </div>
    <div>
        <label for="prix">Prix :</label>
        <input type="text" name="prix" value="<?=$produit["prix"];?>">
    </div>
    <div>
        <label for="dispo">Disponibilité :</label>
        <input type="checkbox" name="dispo" value="1">
    </div>
    <div>
        <label for="sonMP3">Ajouter un son au format MP3 :</label>
        <input type="file" name="fichier" value="<?=$produit["sonMP3"];?>">
    </div>
    <input type="submit" value="Continuer">
</form>