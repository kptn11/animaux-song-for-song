
<!-- formulaire pour créer de l'animal -->
<form enctype="multipart/form-data" action="../../control/createProduit.php" method = "post" >
    <div>
        <label for="nom">Nom de l'animal :</label>
        <input type="text" name="nom">
    </div>
    <div>
        <label for="image">URL de l'image :</label>
        <input type="text" name="imgURL">
    </div>
    <div>
        <label for="info">Breves infos :</label>
        <input type="text" name="info">
    </div>
    <div>
        <label for="prix">Prix :</label>
        <input type="number" name="prix" step="0.1">
    </div>
    <div>
        <label for="dispo">Disponibilité :</label>
        <input type="checkbox" name="dispo">
    </div>
    <div>
        <label for="sonMP3">Ajouter un son au format MP3 :</label>
        <input type="file" name="fichier">
    </div>
    <input type="submit" name="continuer" value="Continuer">
</form>