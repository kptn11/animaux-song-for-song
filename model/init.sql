-- Efface la BDD si elle existe
DROP database if exists Zanimaux;
-- Créer la BDD
create database Zanimaux;
-- Utiliser  la BDD
use Zanimaux;


-- Creation des tables de la BDD

create table produit(
    id integer not null auto_increment primary key,
    nom varchar(255) not null,
    imgURL varchar(800) not null,
    info varchar(800) not null,
    prix int not null,
    dispo boolean default 1,
    sonMP3 varchar(800) 
);
INSERT INTO `produit` (`id`, `nom`, `imgURL`, `info`, `prix`, `dispo`, `sonMP3`) VALUES
(1, 'Lion', 'https://cdn.shopify.com/s/files/1/0267/0788/6159/files/lion-rugissant_0e6b6d51-c925-4748-9284-3640a3f73a9f_1024x1024.jpg?v=1580554389', 'Rugissement du Lion', 25, 1, '../MP3/newlionRugissant.mp3'),
(2, 'Loup', 'https://www.nosanimos.com/wp-content/uploads/2018/02/loup-qui-hurle-300x225.jpg', 'Hurlment du loup', 200, 1, '../MP3/newloup.mp3'),
(3, 'Cerf', 'https://www.peuple-animal.com/data/document/2/1554.800.jpg', 'Brâme du cerf', 200, 1, '../MP3/newcerf.mp3'),
(4, 'Baleine', 'https://mrmondialisation.org/wp-content/uploads/2022/02/48287642607-5fd13863ba-k-1.jpg', 'Chant de la Baleine', 400, 1, '../MP3/newbaleine.mp3'),
(5, 'Singe', 'https://static.nationalgeographic.fr/files/styles/image_3200/public/kanyawara20-20kibale20national20park20uganda_20111019_6355.jpg?w=1600&h=900', 'Grogrement du Babouin', 200, 1, '../MP3/newgrogrementBabouin.mp3'),
(6, 'Tigre', 'https://img.pixers.pics/pho_wat(s3:700/FO/31/58/18/22/700_FO31581822_91976d88b1f90b0fef4c253ef71d956d.jpg,700,508,cms:2018/10/5bd1b6b8d04b8_220x50-watermark.png,over,480,458,jpg)/oreillers-de-corps-gros-plan-d-39-un-tigre-rugissant.jpg.jpg', 'Feulement du tigre', 400, 1, '../MP3/newSFB-tigre-4.mp3'),
(7, 'Pigeon', 'https://www.oiseaux.net/photos/frederic.pelsy/images/pigeon.ramier.frpe.2g.jpg', 'Chant du Pigeon ramier', 30, 1, '../MP3/newsf-pigeon-ramier-chant-03.mp3'),
(8, 'Pie', 'https://lemagdesanimaux.ouest-france.fr/images/dossiers/2022-02/mini/pie-voleuse-063831-650-400.jpg', 'Chant de la Pie', 40, 1, '../MP3/newsf_pie_01.mp3'),
(9, 'Perroquet', 'https://www.parrotworld.fr/sites/default/files/styles/famille_animaux_header/public/media/image/Perroquet_Ara_Macao_157318289_gi_A.jpg?itok=DjIutVPF', 'Chant du Perroquet', 100, 1, '../MP3/newsf_peroquets.mp3');

create table client(
    id integer not null auto_increment primary key,
    nom varchar(255) not null,
    mail varchar(800) not null,
    tel integer not null
);

create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum('panier', 'validée') default 'panier'
);

-- Creation de la table associative

-- table produit-client -> panier
create table panier(
    id_commande int,
    id_produit int,
    primary key (id_commande, id_produit)
);

