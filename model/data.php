<?php
// partie du code permettant l'affichage du debug ! ça peut être utile !!!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
//___________________________________________________________________________________________________________________
//connection a la base de données
$host = '127.0.0.1'; //adresse ip 
$db   = 'Zanimaux';
$user = 'root';
$pass = ''; 
$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
//________________________________________________________________________________________________________________________
//Fonctions du CRUD 

// CRUD des Zanimaux

//Créer un Zanimaux
function createproduit($nom, $imgURL, $info, $prix, $dispo, $sonMP3){
    global $pdo;
    $req = $pdo->prepare("INSERT into produit (nom, imgURL, info, prix, dispo, sonMP3) values(?, ?, ?, ?, ?, ?);");
    $req->execute([$nom, $imgURL, $info, $prix, $dispo, $sonMP3]);   
};

function readallproduit(){
    global $pdo;
    $req = $pdo->query("SELECT * from produit");
    return $req->fetchAll(); 
};

function readproduitbyid($id){
    global $pdo;
    $req = $pdo->prepare("SELECT * from produit where id =?");
    $req ->execute([$id]);
    return $req -> fetchAll();    
};

function updateproduit($nom, $imgURL, $info, $prix, $dispo, $sonMP3, $id){
    global $pdo;
    $req = $pdo->prepare("UPDATE produit set nom =?, imgURL=?, info =?,prix=?, dispo=?, sonMP3=? where id=?;");
    $req->execute([$nom, $imgURL, $info, $prix, $dispo, $sonMP3, $id]);   
};

function deleteproduit($id){
    global $pdo;
    $req = $pdo->prepare("DELETE from produit where id=?;");
    $req->execute([$id]);    
};
//____________________________________________________________________________________________________________________
//read panier

function readAllPanier(){
    global $pdo;
    $req=$pdo->query("SELECT * from panier where id_commande=?");
    $req->execute([$_COOKIE['id_cmd']]);
    return $req->fetchAll();
};

function deleteproduitpanier($id_produit){
    global $pdo;
    $req = $pdo->prepare("DELETE from panier WHERE id_produit=?");
    $req->execute([$id_produit]);
};